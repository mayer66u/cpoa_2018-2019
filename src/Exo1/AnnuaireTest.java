package Exo1;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AnnuaireTest {
	
	Annuaire a;
	@Before
	public void setUp() throws Exception {
		a=new Annuaire();
		a.adjonction("2", "01234");
		a.adjonction("1", "56789");
	}


	@Test
	public void testDomaine() {
		assertEquals("Val attendu: ", "12", a.domaine());
	}

	@Test
	public void testAcces() {
		assertEquals("La valeur attendu: ", "01234", a.acces("2"));
	}
	
	@Test
	public void testAccesNull() {
		assertEquals("La valeur attendu: ", null, a.acces("3"));
	}
	

	@Test
	public void testSuppression() {
		a.suppression("1");
		assertEquals("La valeur devrait etre","2",a.domaine());
	}
	
	@Test
	public void testSuppressionInexistant() {
		a.suppression("3");
		assertEquals("La valeur devrait etre","12",a.domaine());
	}

	@Test
	public void testChangement() {
		a.changement("1", "1010");
		assertEquals("La valeur devrait �tre: ", "1010", a.acces("1"));
	}
	
	@Test
	public void testChangementVide() {
		a.changement("3", "1010");
		assertEquals("La valeur devrait �tre la m�me ", "56789", a.acces("1"));
		assertEquals("La valeur devrait �tre la m�me ", "01234", a.acces("2"));
	}

}
