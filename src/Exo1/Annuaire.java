package Exo1;
import java.util.HashMap;
import java.util.Set;

public class Annuaire extends HashMap<String, String> {

	public Annuaire() {
		super();
	}

	public String domaine(){
		String res="";
		Set<String> cle = this.keySet();
		for(String nom : cle){
			res += nom;
		}
		return res;
	}
	
	public String acces(String nom){
		String res = null;
		if(this.containsKey(nom)){
			res = this.get(nom);
		}
		return res;
	}
	
	public void adjonction(String nom, String numero){
		if(!(this.containsKey(nom))){
			this.put(nom, numero);
		}
	}
	
	public void suppression(String nom){
		if(this.containsKey(nom)){
			this.remove(nom);
		}
	}
	
	public void changement(String nom, String numero){
		if(this.containsKey(nom)){
			this.put(nom, numero);
		}
	}
}
